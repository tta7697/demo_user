<?php
include('../../controller/UserController.php');
$users = new UserController();
$array = $users->update();
$array = $users->index();
?>
<html>
<head>
    <!------------------------------------------------------------------------------------------------->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <!------------------------------------------------------------------------------------------------->
    <style>
        body {
            font-size: 19px;
        }

        table {
            width: 50%;
            margin: 30px auto;
            border-collapse: collapse;
            text-align: left;
        }

        tr {
            border-bottom: 1px solid #cbcbcb;
        }

        th, td {
            border: none;
            height: 30px;
            padding: 2px;
        }

        tr:hover {
            background: #F5F5F5;
        }

        form {
            width: 45%;
            margin: 50px auto;
            text-align: left;
            padding: 20px;
            border: 1px solid #bbbbbb;
            border-radius: 5px;
        }

        .input-group {
            margin: 10px 0px 10px 0px;
        }

        .input-group label {
            display: block;
            text-align: left;
            margin: 3px;
        }

        .input-group input {
            height: 30px;
            width: 93%;
            padding: 5px 10px;
            font-size: 16px;
            border-radius: 5px;
            border: 1px solid gray;
        }

        .btn {
            padding: 10px;
            font-size: 15px;
            color: white;
            background: #5F9EA0;
            border: none;
            border-radius: 5px;
        }

        .edit_btn {
            text-decoration: none;
            padding: 2px 5px;
            background: #2E8B57;
            color: white;
            border-radius: 3px;
        }

        .del_btn {
            text-decoration: none;
            padding: 2px 5px;
            color: white;
            border-radius: 3px;
            background: #800000;
        }

        .msg {
            margin: 30px auto;
            padding: 10px;
            border-radius: 5px;
            color: #3c763d;
            background: #dff0d8;
            border: 1px solid #3c763d;
            width: 50%;
            text-align: center;
        }

        a {
            color: black;
        }
    </style>
</head>
<body>
<div class="container">
    <form method="post" action="">
        <div class="input-group">
            <label>Name</label>
            <input type="text" name="name" value="">
        </div>
        <div class="input-group">
            <label>Username</label>
            <input type="text" name="username" value="">
        </div>
        <!--        <div class="input-group">-->
        <!--            <label>Telephone</label>-->
        <!--            <input type="text" name="telephone" value="">-->
        <!--        </div>-->
        <!--        <div class="input-group">-->
        <!--            <label>email</label>-->
        <!--            <input type="text" name="email" value="">-->
        <!--        </div>-->
        <div class="input-group">
            <button type="submit" name="update" class="btn btn-primary">Save</button>
        </div>
    </form>
    <table width="100%" class="table table-bordered table-striped" style="border-collapse: collapse; margin-top: 50px">
        <thead>
        <tr class="center">
            <th width="10%" style="text-align: center;vertical-align: middle">Id</th>
            <th width="20%" style="text-align: center;vertical-align: middle">Name</th>
            <th width="20%" style="text-align: center;vertical-align: middle">User Name</th>
            <!--            <th width="20%" style="text-align: center;vertical-align: middle">Telephone</th>-->
            <!--            <th width="20%" style="text-align: center;vertical-align: middle">Email</th>-->
            <th width="20%" style="text-align: center;vertical-align: middle">Delete</th>
        </tr>
        <?php
        if (isset($array)) {
            foreach ($array as $arr) {
                echo '<tr>
             <td>' . $arr["id"] . '</td>
             <td>' . $arr["username"] . '</td>
             <td>' . $arr["name"] . '</td>
             <td>' . '<a href="' . 'http://localhost/Demo3/view/user/deleteUser.php?id=' . $arr['id'] . '">' . 'Delete' . '</a>' . '</td>
             </tr>';
            }
        }
        ?>
        </thead>
    </table>
</div>


</body>
</html>


