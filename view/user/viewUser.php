<?php
include('../../controller/UserController.php');
$users = new UserController();
$array = $users->view();
?>

<html>
<head>
    <!------------------------------------------------------------------------------------------------->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <!------------------------------------------------------------------------------------------------->
    <style>
        td {
            text-align: center;
        }

        td:hover {
            background: #dee2e6;
        }
    </style>
</head>
<div class="container" style="margin-top: 100px">
    <body>
    <table width="100%" class="table table-bordered table-striped" style="border-collapse: collapse; margin-top: 50px">
        <thead>

        <tr class="center">
            <th width="10%" style="text-align: center;vertical-align: middle">Id</th>
            <th width="10%" style="text-align: center;vertical-align: middle">Name</th>
            <th width="20%" style="text-align: center;vertical-align: middle">Số điện thoại</th>
            <th width="20%" style="text-align: center;vertical-align: middle">Email</th>
        </tr>
        <?php
        if (is_array($array)) {
            foreach ($array as $arr) {
                echo '<tr>
            <td>' . $arr["id"] . '</td>
            <td>' . $arr["name"] . '</td>
            <td>' . $arr["telephone"] . '</td>
            <td>' . $arr["email"] . '</td>
      </tr>';
            }
        }
        ?>
        </thead>
    </table>
    </body>
</div>
</html>


