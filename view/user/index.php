<?php
include('../../controller/UserController.php');
$users = new UserController();
$array = $users->index();
//$array = $users->delete();
?>

<html>
<head>
    <!------------------------------------------------------------------------------------------------->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <!------------------------------------------------------------------------------------------------->
    <style>
        td {
            text-align: center;
        }

        td:hover {
            background: #dee2e6;
        }

        a {
            color: black;
            text-decoration: none;
        }
    </style>
</head>
<div class="container" style="margin-top: 100px">
    <body>
    <table width="100%" class="table table-bordered table-striped" style="border-collapse: collapse; margin-top: 50px">
        <thead>

        <tr class="center">
            <th width="10%" style="text-align: center;vertical-align: middle">Id</th>
            <th width="20%" style="text-align: center;vertical-align: middle">User Name</th>
            <th width="20%" style="text-align: center;vertical-align: middle">Name</th>
            <th width="20%" style="text-align: center;vertical-align: middle">Delete</th>
            <th width="20%" style="text-align: center;vertical-align: middle">Update</th>
        </tr>
        <?php
        if (is_array($array)) {
            foreach ($array as $arr) {
                echo '<tr>
            <td>' . $arr["id"] . '</td>
            <td>' . $arr["username"] . '</td>
            <td>' . '<a href="' . 'http://localhost/Demo3/view/user/viewUser.php?id=' . $arr['id'] . '">' . $arr["name"] . '</a>' . '</td>
            <td>' . '<a href="' . 'http://localhost/Demo3/view/user/deleteUser.php?id=' . $arr['id'] . '">' . 'Delete' . '</a>' . '</td>
            <td>' . '<a href="' . 'http://localhost/Demo3/view/user/updateUser.php?id=' . $arr['id'] . '">' . 'Update' . '</a>' . '</td>

        </tr>';
            }
        }
        ?>
        <script>
            document.getElementById("view_user").addEventListener("click", myFunction);

            function myFunction() {
                document.getElementById("view_user").innerHTML = "YOU CLICKED ME!";
            }
        </script>
        </thead>
    </table>
    <a href="http://localhost/Demo3/view/user/addUser.php" target="_blank" rel="follow, index"
       style="margin: 20px 0 0 20px">
        <button class="btn btn-primary" data-toggle="modal" data-target="#myModal" id="button1">
            Add new User
        </button>
    </a>
    <a href="http://localhost/Demo3/view/user/searchUser.php" target="_blank" rel="follow, index"
       style="margin: 20px 0 0 20px">
        <button class="btn btn-primary" data-toggle="modal" data-target="#myModal" id="button1">
            Search User
        </button>
    </a>
    </body>
</div>
</html>


