<?php

include('../../config/config.php');

class user
{
    public $connectDB;

    function __construct()
    {
        $db = new connectDB();
        $this->connectDB = $db->connect();

    }

    public function select()
    {
        $sql = "SELECT id, name, username, telephone, email FROM user";
        $result = $this->connectDB->query($sql);
        $arrUser = [];
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $arrUser[] = $row;
            }
            return $arrUser;
        }
        $this->connectDB->close();
    }

    public function add($name, $username, $telephone, $email)
    {
        $sql = "INSERT INTO user (name, username, telephone, email) VALUES ('$name','$username','$telephone','$email')";
        $check_insert = $this->connectDB->query($sql);
        return $check_insert;
    }

    public function search($name, $username)
    {
        $sql = "SELECT * FROM user WHERE name LIKE '$name' OR username LIKE '$username'";
//        var_dump($name,$username);
        $check_search = $this->connectDB->query($sql);
        $arrUser = [];
        if ($check_search->num_rows > 0) {
            while ($row = $check_search->fetch_assoc()) {
                $arrUser[] = $row;
            }
        }
        $this->connectDB->close();
        return $arrUser;
    }

    public function view($id)
    {
        $sql = "SELECT * FROM user WHERE id= $id ";
        $check_view = $this->connectDB->query($sql);
        $arrUser = [];
        if ($check_view->num_rows > 0) {
            while ($row = $check_view->fetch_assoc()) {
                $arrUser[] = $row;
            }
        }
        $this->connectDB->close();
        return $arrUser;
    }

    public function delete($id)
    {
        $sql = "DELETE FROM user WHERE id = $id";
        $check_delete = $this->connectDB->query($sql);
        $this->connectDB->close();
        return $check_delete;
    }

    public function update($id, $name, $username)
    {
        $sql = "UPDATE user SET name ='$name', username='$username' WHERE id= '$id'";
        $check_update = $this->connectDB->query($sql);
//        var_dump($check_update);die();
        $this->connectDB->close();
        return $check_update;
    }
}

