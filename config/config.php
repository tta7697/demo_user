<?php

class connectDB
{
    public function __construct()
    {
        $this->connect();
    }

    public function connect()
    {
        $conn = mysqli_connect('localhost', 'root', '', 'ewallet');
        mysqli_set_charset($conn, 'utf8');

        if (!$conn) {
            die("Connection failed: " . mysqli_connect_error());
        }
        return $conn;
    }
}

?>
