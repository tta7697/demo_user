<?php

include('../../model/User.php');

class UserController
{
    public function index()
    {
        $users = new user();
        $array = $users->select();
        return $array;
    }

    public function add()
    {
        if (isset($_POST['save'])) {
            $name = $_POST['name'];
            $username = $_POST['user_name'];
            $telephone = $_POST['telephone'];
            $email = $_POST['email'];
            $users = new user();
            $check = $users->add($name, $username, $telephone, $email);
            if (!$check) {
                header("Location: http://localhost/Demo3/view/user/errorr.php");
            } else {
                header("Location: http://localhost/Demo3/view/user/addUser.php");
            }

        }

    }

    public function search()
    {
        if (isset($_POST['search'])) {
            $name = $_POST['name'];
            $username = $_POST['username'];
            $users = new user();
            $array = $users->search($name, $username);
            return $array;
        } else {
            $users = new user();
            $array = $users->select();
            return $array;
        }
    }

    public function view()
    {
        $users = new user();
        $id = $_GET['id'];
        $array = $users->view($id);
        return $array;
    }

    public function delete()
    {
        $id = $_GET['id'];
        $users = new user();
        $check = $users->delete($id);
//        var_dump($check);die();
        if ($check) {
            header("Location: http://localhost/Demo3/view/user/index.php");
        }
        return $check;
    }

    public function update()
    {

        if (isset($_REQUEST['update'])) {
            $id = $_GET['id'];
            $name = $_POST['name'];
            $username = $_POST['username'];
//            $telephone = $_POST['telephone'];
//            $email = $_POST['email'];
            $users = new user();
            $check = $users->update($id, $name, $username);
//            var_dump($check);die();
            if (!$check) {
                header("Location: http://localhost/Demo3/view/user/updateUser.php");
            }
        }
    }


}



